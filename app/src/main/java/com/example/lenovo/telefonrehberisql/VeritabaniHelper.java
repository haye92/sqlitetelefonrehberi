package com.example.lenovo.telefonrehberisql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by LENOVO on 20.04.2017.
 */

public class VeritabaniHelper extends SQLiteOpenHelper{

    private static final int DatabaseVersion = 1;
    private static final String DatabaseName = "Deneme.db";

    public VeritabaniHelper(Context context) {
        super(context, DatabaseName, null, DatabaseVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.beginTransaction();

            // burada tablolar oluşturulur.
            //db.execSQL("CREATE TABLE Task (id INTEGER PRIMARY KEY AUTOINCREMENT, text TEXT NOT NULL, completed BOOLEAN DEFAULT 0);");

            db.execSQL("CREATE TABLE Numaralar (isim TEXT, numara TEXT);");

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
