package com.example.lenovo.telefonrehberisql;

/**
 * Created by LENOVO on 20.04.2017.
 */

public class KisiPOJO {
        private String isim;
        private String numara;

    KisiPOJO(String isim, String numara) {
        setIsim(isim);
        setNumara(numara);
    }

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public String getNumara() {
        return numara;
    }

    public void setNumara(String numara) {
        this.numara = numara;
    }
}
