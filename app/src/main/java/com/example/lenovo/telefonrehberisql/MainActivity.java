package com.example.lenovo.telefonrehberisql;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //tumunuSil();
        veriEkle("deneme", "123");

        listele();

        Button b = (Button) findViewById(R.id.button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText isimText = (EditText) findViewById(R.id.editText);
                EditText numaraText = (EditText) findViewById(R.id.editText2);

                String isim = isimText.getText().toString();
                String numara = numaraText.getText().toString();

                veriEkle(isim, numara);
                listele();
            }
        });
    }

    protected void listele() {
        ArrayList sonuclar = veriCek();

        TextView tw = (TextView) findViewById(R.id.textView);

        String text = "";
        for (int i=0; i<sonuclar.size();i++) {
            KisiPOJO k = (KisiPOJO) sonuclar.get(i);
            text += k.getIsim() + "\t " + k.getNumara() + "\n";
        }
        tw.setText(text);
    }


    protected void veriEkle(String isim, String numara) {

        ContentValues insertValues = new ContentValues();
        // Burada ilk parametre tablo sütununun ismi, ikinci ise eklenecek değer.
        insertValues.put("isim", isim);
        insertValues.put("numara", numara);


        VeritabaniHelper dbHelper = null;
        SQLiteDatabase db = null;
        try {

            dbHelper = new VeritabaniHelper(this);
            // Kayıt eklemek için kullanacağımızdan yazma destekli bir database bağlantısı açıyoruz.
            db = dbHelper.getWritableDatabase();

            // Daha önce yarattığımız tablomuza yukarıda hazırladığımız verileri ekliyoruz.
            // Kullandığımız insert metodu bize başarılı olması durumunda satırın id'sini verecek.
            // Hata olması durumunda ise -1 dönecek.
            long id = db.insert("Numaralar", null, insertValues);

            if (-1 == id) {
                System.out.println("Başarısız");
            }else {
                System.out.println("OK");
            }
        } catch (SQLiteException e) {
            Log.e("Task DB", "Error on inserting record.", e);
        } finally {
            // Yazma işi bittiğinde mutlaka database bağlantısını kapatmalıyız.
            if (null != db) {
                db.close();
            }

            // Helper ile açık kalan tüm bağlantıları sonlandırabiliriz.
            if (null != dbHelper) {
                dbHelper.close();
            }
        }
    }


    protected void tumunuSil() {
        // Silecegimiz kayitlari sinirlandirmak i￧in kullandigimiz where i￧inde
        // verdigimiz parametrelerin degerlerini bu sekilde sagliyoruz.

        VeritabaniHelper dbHelper = null;
        SQLiteDatabase db = null;
        try {
            // Helper objemizi olusturuyoruz.
            dbHelper = new VeritabaniHelper(this);
            // Kayit eklemek i￧in kullanacagimizdan yazma destekli bir
            // database baglantisi a￧iyoruz.
            db = dbHelper.getWritableDatabase();

            // Tabloya daha ￶nce ekledigimiz kayitlari id'leri yardimi ile siliyoruz.
            // Geriye silinen kayit sayisi d￶nd￼r￼lecektir.
            // Eger t￼m kayitlari silmek isteseydik 2. parametrede "1" yazardik.
            int deletedRowCount = db.delete("Numaralar", null, null);

            // Islem sonucunu bu sekilde kontrol edebiliriz.
            if (deletedRowCount > 0) {
                Log.d("Task DB", deletedRowCount + " record(s) deleted.");
            }
        } catch (SQLiteException e) {
            Log.e("Task DB", "Error on deleting record(s).", e);
        } finally {
            // Yazma isi bittiginde mutlaka database baglantisini
            // kapatmaliyiz.
            if (null != db) {
                db.close();
            }

            // Helper ile a￧ik kalan t￼m baglantilari kapatabiliriz.
            if (null != dbHelper) {
                dbHelper.close();
            }
        }
    }


    public ArrayList<KisiPOJO> veriCek() {
        // Çekeceğimiz kayıtları sınırlandırmak için kullandığımız where içinde verdiğimiz parametrelerin değerlerini bu şekilde sağlıyoruz.
        String[] whereArgs = new String[1];
        whereArgs[0] = String.valueOf(0);

        VeritabaniHelper dbHelper = null;
        SQLiteDatabase db = null;
        Cursor cursor = null;

        try {
            // Helper objemizi oluşturuyoruz.
            dbHelper = new VeritabaniHelper(this);

            // Kayıt çekmek için kullanacağımızdan okuma destekli bir database bağlantısı açıyoruz.
            db = dbHelper.getReadableDatabase();

            // Kayıt çekmek için query overload methodunun sizin için uygun olanını kullanabilirsiniz.
            // İlk parametere tablo ismi.
            // İkinci parametre çekilecek sütunlar. null verilmesi halinde tüm sütunlar çekilir.
            // Üçüncü parametre where koşulları
            // Dördüncü parametre where koşullarında verilen paramertelerin değerleri.
            // Beşinci parametre SQL groupBy
            // Altıncı parametre SQL having
            // Son paramerte ise SQL orderBy
            // Burada yazdığımız "SELECT * FROM Task WHERE completed == ?;" SQL sözcüğüne denktir.
            // Geriye bir cursor objesi döner. Bunu kullanarak sonuç içinde gezebiliriz.
            cursor = db.query("Numaralar", null, null, null, null, null, null);

            // Sonuçlar içinde ilk satıra gidiyoruz.
            cursor.moveToFirst();

            // Eğer bu son satırdan sonraysa elimize hiç sonuç dönmemiş demektir
            if (cursor.isAfterLast()) {
                // Burada sonuç alamadığımız sorgu ile ilgili gerekli işlemler yapılır.
                return null;
            }

            final int isimColumnIndex = cursor.getColumnIndex("isim");
            final int numaraColumnIndex = cursor.getColumnIndex("numara");

            final ArrayList<KisiPOJO> sonuclar = new ArrayList(cursor.getCount());
            do {
                // Sonuçlar içinde aşağıdaki gibi geziyoruz. Sonuçlarla istediğimiz herhangi bir işi burada gerçekleştiriyoruz.
                KisiPOJO pojo = new KisiPOJO(
                        cursor.getString(isimColumnIndex),
                        cursor.getString(numaraColumnIndex));

                sonuclar.add(pojo);
            } while (cursor.moveToNext());

            if (sonuclar.size() > 0) {
                // Some work...
            }
            return sonuclar;
        } finally {
            // Çektiğimiz data ile işimiz bittiğinde mutlaka cursor objesini kapatmalıyız.
            if (null != cursor) {
                cursor.close();
            }

            // Yazma işi bittiğinde mutlaka database bağlantısını kapatmalıyız.
            if (null != db) {
                db.close();
            }

            // Helper ile açık kalan tüm bağlantıları kapatabiliriz.
            if (null != dbHelper) {
                dbHelper.close();
            }
        }
    }
}
